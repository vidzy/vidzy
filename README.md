# Vidzy has moved to Github!
# https://github.com/vidzy-social/vidzy

<br><br><br><br><br><br><br><br>

<div align="center">
<img src="static/logo.png">
<h1>Vidzy</h1>
A free and open source alternative to TikTok

<a href="https://vidzy.codeberg.page/">Website</a>
&nbsp;•&nbsp;
<a href="https://github.com/vidzy-social/vidzy">Github Repo</a>

![License: AGPL-v3.0](./license_badge.svg)
</div>

<br><br>